//Write a program to add two user input numbers using one function.
#include <stdio.h>
int main() 
{    

    int number1, number2, result;
    
    printf("Enter two integers: ");
    scanf("%d %d", &number1, &number2);
    result = ADD(number1, number2);      
    printf("\n Sum is \t %d + %d = %d", number1, number2, result);
    return 0;
}

int ADD(int a, int b)
{
int Sum;
Sum=a+b;
return(Sum);
}